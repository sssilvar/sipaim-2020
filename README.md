# sipaim-2020

Website for SIPAIM2020

## Run app locally as container

Just execute:

```bash
docker run --rm -p 8000:80 -v $(pwd):/var/www/html php:apache
```
